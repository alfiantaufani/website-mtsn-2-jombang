<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">Menu Utama</li>
        <li class="<?php echo $this->uri->segment(2) == 'dashboard' ? 'active': '' ?>">
          <a href="<?php echo base_url().'admin/dashboard'?>">
            <i class="fa fa-home"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
        <li class="treeview <?php echo $this->uri->segment(2) == 'tulisan' ? 'active': '' ?>">
          <a href="#">
            <i class="fa fa-newspaper-o"></i>
            <span>Blog</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url().'admin/tulisan'?>"><i class="fa fa-list"></i> List Blog</a></li>
            <li><a href="<?php echo base_url().'admin/tulisan/add_tulisan'?>"><i class="fa fa-thumb-tack"></i> Post Blog</a></li>
            <li><a href="<?php echo base_url().'admin/kategori'?>"><i class="fa fa-wrench"></i> Kategori</a></li>
          </ul>
        </li>
        <li class="<?php echo $this->uri->segment(2) == 'agenda' ? 'active': '' ?>">
          <a href="<?php echo base_url().'admin/agenda'?>">
            <i class="fa fa-calendar"></i> <span>Agenda</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
        <li class="<?php echo $this->uri->segment(2) == 'pengumuman' ? 'active': '' ?>">
          <a href="<?php echo base_url().'admin/pengumuman'?>">
            <i class="fa fa-bullhorn"></i> <span>Pengumuman</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
        <li class="<?php echo $this->uri->segment(2) == 'files' ? 'active': '' ?>">
          <a href="<?php echo base_url().'admin/files'?>">
            <i class="fa fa-download"></i> <span>Download</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
        <li class="treeview <?php echo $this->uri->segment(2) == 'album' ? 'active': '' ?>">
          <a href="#">
            <i class="fa fa-camera"></i>
            <span>Gallery</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url().'admin/album'?>"><i class="fa fa-clone"></i> Album</a></li>
            <li><a href="<?php echo base_url().'admin/galeri'?>"><i class="fa fa-picture-o"></i> Photos</a></li>
          </ul>
        </li>

        <li class="<?php echo $this->uri->segment(2) == 'guru' ? 'active': '' ?>">
          <a href="<?php echo base_url().'admin/guru'?>">
            <i class="fa fa-graduation-cap"></i> <span>Data Guru</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
        <li class="<?php echo $this->uri->segment(2) == 'siswa' ? 'active': '' ?>">
          <a href="<?php echo base_url().'admin/siswa'?>">
            <i class="fa fa-users"></i> <span>Data Alumni</span>
          </a>
        </li>
        <li class="treeview <?php echo $this->uri->segment(2) == 'content' ? 'active': '' ?>">
          <a href="#">
            <i class="fa fa-list-ul"></i>
            <span>Main Konten</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url().'admin/content/pages'?>"><i class="fa fa-file"></i> Halaman</a></li>
            <li><a href="<?php echo base_url().'admin/content/menus'?>"><i class="fa fa-th-list"></i> Menu Utama</a></li>
            <li><a href="<?php echo base_url().'admin/content/submenus'?>"><i class="fa fa-list-ol"></i> Sub Menu</a></li>
          </ul>
        </li>
        <li class="<?php echo $this->uri->segment(2) == 'slider' ? 'active': '' ?>">
          <a href="<?php echo base_url().'admin/sliders/slider'?>">
            <i class="fa fa-columns"></i> <span>Slider</span>
          </a>
        </li>

        <!-- <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i>
            <span>Kesiswaan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url().'admin/siswa'?>"><i class="fa fa-users"></i> Data Siswa</a></li>
            <li><a href="#"><i class="fa fa-star-o"></i> Prestasi Siswa</a></li>

          </ul>
        </li> -->
        
        <li class="<?php echo $this->uri->segment(2) == 'inbox' ? 'active': '' ?>">
          <a href="<?php echo base_url().'admin/inbox'?>">
            <i class="fa fa-envelope"></i> <span>Pesan Masuk</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-green"><?php echo $jum_pesan;?></small>
            </span>
          </a>
        </li>

        <li class="<?php echo $this->uri->segment(2) == 'komentar' ? 'active': '' ?>">
          <a href="<?php echo base_url().'admin/komentar'?>">
            <i class="fa fa-comments"></i> <span>Komentar</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-green"><?php echo $jum_comment;?></small>
            </span>
          </a>
        </li>
        <li class="<?php echo $this->uri->segment(2) == 'pengguna' ? 'active': '' ?>">
          <a href="<?php echo base_url().'admin/pengguna'?>">
            <i class="fa fa-users"></i> <span>Pengguna</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
        <li class="<?php echo $this->uri->segment(2) == 'settings' ? 'active': '' ?>">
          <a href="<?php echo base_url().'admin/settings/settings'?>">
            <i class="fa fa-gear"></i> <span>Pengaturan</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
        <li>
          <a href="<?php echo base_url('admin/login/logout')?>">
            <i class="fa fa-lock"></i> <span>Keluar</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>


      </ul>
    </section>
    <!-- /.sidebar -->
</aside>