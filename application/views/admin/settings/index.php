<!--Counter Inbox-->
<?php
error_reporting(0);
    $query=$this->db->query("SELECT * FROM tbl_inbox WHERE inbox_status='1'");
    $query2=$this->db->query("SELECT * FROM tbl_komentar WHERE komentar_status='0'");
    $jum_comment=$query2->num_rows();
    $jum_pesan=$query->num_rows();
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>MTsN 2 Jombang | <?php echo $title;?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="shorcut icon" type="text/css" href="<?php echo base_url().'assets/images/favicon.png'?>">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/bootstrap/css/bootstrap.min.css'?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/font-awesome/css/font-awesome.min.css'?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/datatables/dataTables.bootstrap.css'?>">
  <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/daterangepicker/daterangepicker.css'?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/dist/css/AdminLTE.css'?>">
  <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/daterangepicker/daterangepicker.css'?>">
  <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/timepicker/bootstrap-timepicker.min.css'?>">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/datepicker/datepicker3.css'?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/dist/css/skins/_all-skins.min.css'?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.css'?>"/>

</head>
<body class="hold-transition skin-blue sidebar-mini fixed">
<div class="wrapper">

   <?php
    $this->load->view('admin/v_header');
    $this->load->view('admin/sidebar');
  ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $title;?>
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?php echo $title;?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <!-- /.box-header -->
            <div class="box-header">
              <!-- <a href="<?php echo base_url('admin/content/pages/create')?>" class="btn btn-success btn-flat"><span class="fa fa-plus"></span> Add <?php echo $title;?></a> -->
            </div>
                <div class="box-body">
                    <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <!-- <li class="active"><a href="#homepage" data-toggle="tab"><i class="fa fa-home"></i> Halaman Utama</a></li> -->
                        <li class="active"><a href="#kontak" data-toggle="tab"><i class="fa fa-phone-square"></i> Kontak</a></li>
                        <li><a href="#sosmed" data-toggle="tab"><i class="fa fa-facebook-square"></i> Sosial Media</a></li>
                        <li><a href="#logo" data-toggle="tab"><i class="fa fa-image"></i> Logo</a></li>
                        <li><a href="#sistem" data-toggle="tab"><i class="fa fa-laptop"></i> Sistem</a></li>
                        <li><a href="#seo" data-toggle="tab"><i class="fa fa-globe"></i> Seo</a></li>
                    </ul>
                    <div class="tab-content">
                        <?php foreach ($data->result_array() as $set) : ?>
                        <!-- <div class="tab-pane active" id="homepage">
                            <form action="<?= base_url('admin/settings/settings/update') ?>" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="type" value="homepage">
                                <h4>Sambutan Kepala Sekolah</h4>
                                <div class="form-group">
                                    <label>Nama Kepala Sekolah</label>
                                    <input type="text" class="form-control" name="nama_kepsek" value="<?= $set['nama_kepsek'] ?>" placeholder="Nama Kepala Sekolah">
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="box-body box-profile">
                                            <img class="attachment-img" src="<?= base_url() ?>theme/images/kepsek/<?= $set['foto_kepsek'] ?>" alt="User profile picture">
                                            <input type="hidden" name="foto_kepsek" value="<?= $set['foto_kepsek'] ?>">
                                        </div>
                                    </div>
                                
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <label>Foto Kepala Sekolah</label>
                                            <input type="file" name="filefoto" class="form-control" placeholder="Foto Kepala Sekolah">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label>Teks Sambutan</label>
                                    <textarea class="form-control" name="text_home" rows="10" placeholder="Teks Sambutan"><?= $set['text_home'] ?></textarea>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </form>
                        </div> -->
                        
                        <div class="tab-pane active" id="kontak">
                            <form action="<?= base_url('admin/settings/settings/update') ?>" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="type" value="kontak">
                                <div class="form-group">
                                    <label>Email Sekolah</label>
                                    <input type="email" class="form-control" name="email" value="<?= $set['email'] ?>" placeholder="Email Sekolah">
                                </div>
                                <div class="form-group">
                                    <label>Telepon Sekolah</label>
                                    <input type="text" class="form-control" name="no_telephone" value="<?= $set['no_telephone'] ?>" placeholder="Telepon Sekolah">
                                </div>
                                <div class="form-group">
                                    <label>Alamat Sekolah</label>
                                    <textarea class="form-control" name="address" rows="3" placeholder="Alamat Sekolah"><?= $set['address'] ?></textarea>
                                </div>

                                <div class="form-group">
                                    <label>Link Google Map</label>
                                    <textarea class="form-control" name="maps" rows="3" placeholder="Link Google Map"><?= $set['maps'] ?></textarea>
                                </div>
                                <div class="form-group">
                                    <button type="submit" name="save_kontak" value="kontak" class="btn btn-primary">Simpan</button>
                                </div>
                            </from>
                        </div>
                        
                        <div class="tab-pane" id="sosmed">
                            <form action="<?= base_url('admin/settings/settings/update') ?>" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="type" value="sosmed">
                                <div class="form-group">
                                    <label>Facebook</label>
                                    <input type="text" class="form-control" value="<?= $set['facebook'] ?>" name="facebook" placeholder="Facebook">
                                </div>
                                <div class="form-group">
                                    <label>Instagram</label>
                                    <input type="text" class="form-control" value="<?= $set['instagram'] ?>" name="instagram" placeholder="Instagram">
                                </div>
                                <div class="form-group">
                                    <label>Twitter</label>
                                    <input type="text" class="form-control" value="<?= $set['twitter'] ?>" name="twitter" placeholder="Twitter">
                                </div>
                                <div class="form-group">
                                    <button type="submit" name="save_sosmed" value="sosmed" class="btn btn-primary">Simpan</button>
                                </div>
                            </from>
                        </div>
                        
                        <div class="tab-pane" id="logo">
                            <form action="<?= base_url('admin/settings/settings/update') ?>" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="type" value="logo">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="box-body box-profile">
                                            <img class="attachment-img" src="<?= base_url() ?>theme/images/logo/<?= $set['logo'] ?>" alt="User profile picture">
                                        </div>
                                    </div>
                                
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <label>Logo Sekolah</label>
                                            <input type="file" name="filelogo" class="form-control" placeholder="Logo Sekolah">
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" name="save_logo" value="logo" class="btn btn-primary">Simpan</button>
                                        </div>
                                    </div>
                                </div>
                            </from>
                        </div>
                        
                        <div class="tab-pane" id="sistem">
                            <form action="<?= base_url('admin/settings/settings/update') ?>" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="type" value="sistem">
                                <h4>Admin Setting</h4>
                                <div class="form-group">
                                    <label>Admin Title</label>
                                    <input type="text" class="form-control" name="admin_title" value="<?= $set['admin_title'] ?>" placeholder="Admin Title">
                                </div>
                                <div class="form-group">
                                    <label>Admin Footer</label>
                                    <input type="text" class="form-control" name="admin_footer" value="<?= $set['admin_footer'] ?>" placeholder="Admin Footer">
                                </div>
                                <hr>
                                <h4>Site Setting</h4>
                                <div class="form-group">
                                    <label>Site Title</label>
                                    <input type="text" class="form-control" name="site_title" value="<?= $set['site_title'] ?>" placeholder="Site Title">
                                </div>
                                <div class="form-group">
                                    <label>Site Footer</label>
                                    <input type="text" class="form-control" value="<?= $set['site_footer'] ?>" name="site_footer" placeholder="Site Footer">
                                </div>
                                <div class="form-group">
                                    <button type="submit" name="save_sistem" value="sistem" class="btn btn-primary">Simpan</button>
                                </div>
                            </from>
                        </div>
                        
                        <div class="tab-pane" id="seo">
                            <form action="<?= base_url('admin/settings/settings/update') ?>" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="type" value="seo">
                                <div class="form-group">
                                    <label>Deskripsi Site</label>
                                    <textarea class="form-control" name="description"rows="3" placeholder="Deskripsi Site"><?= $set['description'] ?></textarea>
                                </div>
                                <div class="form-group">
                                    <button type="submit" name="save_seo" value="seo" class="btn btn-primary">Simpan</button>
                                </div>
                            </form>
                        </div>
                        <?php endforeach ?>
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <?php $this->load->view('admin/footer');?>
<!-- ./wrapper -->

	<?php foreach ($data->result_array() as $i) :
              $id=$i['id'];
            ?>
	<!--Modal Hapus Pengguna-->
        <div class="modal fade" id="ModalHapus<?php echo $id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                        <h4 class="modal-title" id="myModalLabel">Hapus Agenda</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url().'admin/content/pages/delete'?>" method="post" enctype="multipart/form-data">
                    <div class="modal-body">
							       <input type="hidden" name="kode" value="<?php echo $id;?>"/>
                            <p>Apakah Anda yakin mau menghapus data ini?</p>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Hapus</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
	<?php endforeach;?>




<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url().'assets/plugins/jQuery/jquery-2.2.3.min.js'?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url().'assets/bootstrap/js/bootstrap.min.js'?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url().'assets/plugins/datatables/jquery.dataTables.min.js'?>"></script>
<script src="<?php echo base_url().'assets/plugins/datatables/dataTables.bootstrap.min.js'?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url().'assets/plugins/slimScroll/jquery.slimscroll.min.js'?>"></script>
<script src="<?php echo base_url().'assets/plugins/datepicker/bootstrap-datepicker.js'?>"></script>
<script src="<?php echo base_url().'assets/plugins/timepicker/bootstrap-timepicker.min.js'?>"></script>
<script src="<?php echo base_url().'assets/plugins/daterangepicker/daterangepicker.js'?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url().'assets/plugins/fastclick/fastclick.js'?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url().'assets/dist/js/app.min.js'?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url().'assets/dist/js/demo.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.js'?>"></script>
<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });

    $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
    $('#datepicker2').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
    $('.datepicker3').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
    $('.datepicker4').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
    $(".timepicker").timepicker({
      showInputs: true
    });

  });
</script>
<?php if($this->session->flashdata('msg')=='error'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Error',
                    text: "Proses gagal",
                    showHideTransition: 'slide',
                    icon: 'error',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#FF4859'
                });
        </script>

    <?php elseif($this->session->flashdata('msg')=='succes-update'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Info',
                    text: "Berhasil di update",
                    showHideTransition: 'slide',
                    icon: 'info',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#00C9E6'
                });
        </script>
    <?php elseif($this->session->flashdata('msg')=='success-hapus'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Berhasil dihapus.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
        </script>
    <?php elseif($this->session->flashdata('msg')=='success'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Berhasil ditambah.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
        </script>
    <?php else:?>

    <?php endif;?>
</body>
</html>
