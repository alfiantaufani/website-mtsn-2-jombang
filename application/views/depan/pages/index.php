<?php foreach ($pages->result_array() as $data):?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo $data['title'];?> - MTsN 2 Jombang</title>
    <meta name="description" content="<?php echo $data['description'];?>">
    <meta name="author" content="MTsN 2 Jombang">
    <meta name="keywords" content="<?php echo $data['title'];?>, MTsN 2 Jombang, Madrasah Tsanawiyah Negri, MTsN 2, Jombang, Jawa Timur, Dahrul Ulum Rejoso Jombang">
    <meta property="og:url" content="<?php echo base_url();?>p/<?php echo $data['slug'];?>">
    <meta property="og:site_name" content="<?php echo $data['description'];?>">
    <meta property="og:description" content="<?php echo $data['title'];?>">
    <meta property="og:image" content="<?php echo base_url().'theme/images/logo/logo.png'?>">
    <meta property="twitter:site" content="<?php echo $data['description'];?>">
    <meta property="twitter:site:id" content="">
    <meta property="twitter:card" content="summary">
    <meta property="twitter:description" content="<?php echo $data['description'];?>">
    <meta property="twitter:image:src" content="<?php echo base_url().'theme/images/logo/logo.png'?>">
<?php endforeach;?>

<?php $this->load->view('depan/header'); ?>
<!--//END HEADER -->
<div class="detailed_chart">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-12 chart_bottom">
                <h3 class="text-center" style="color: #fff;"><b><?php echo $title; ?></b></h3>
            </div>
        </div>
    </div>
</div>
<section class="blog-wrap" style="padding-top:70px">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="blog-tiltle_block">
                    <?php foreach ($pages->result_array() as $i):?>
                        <h2><b><?php echo $title;?></b></h2>
                        <div class="blog-icons">
                            <div class="blog-share_block">
                                <h6 style="float: left;">
                                    <a href="#">
                                        <i class="fa fa-user" aria-hidden="true"></i> Penulis : 
                                        <span><?php echo $i['pengguna_nama'];?></span> 
                                    </a> 
                                </h6>
                                <div class="sharePopup"></div>
                            </div>
                        </div>
                        
                        <?php echo $i['content'];?>
                    <?php endforeach;?>
                </div>
            </div>
            <div class="col-md-4">
              <form action="<?php echo site_url('blog/search');?>" method="get">
                  <input type="text" name="keyword" placeholder="Search" class="blog-search" required>
                  <button type="submit" class="btn btn-warning btn-blogsearch">SEARCH</button>
              </form>
                <div class="blog-category_block">
                    <h3>Kategori Artikel</h3>
                    <ul>
                      <?php foreach ($category->result() as $row) : ?>
                        <li><a href="<?php echo site_url('blog/kategori/'.str_replace(" ","-",$row->kategori_nama));?>"><?php echo $row->kategori_nama;?><i class="fa fa-caret-right" aria-hidden="true"></i></a></li>
                      <?php endforeach;?>
                    </ul>
                </div>
                <div class="blog-featured_post">
                    <h3>Artikel Populer</h3>
                  <?php foreach ($populer->result() as $row) :?>
                    <div class="blog-featured-img_block">
                        <img width="35%" src="<?php echo base_url().'assets/images/'.$row->tulisan_gambar;?>" class="img-fluid" alt="blog-featured-img">
                        <h5><a href="<?php echo site_url('artikel/'.$row->tulisan_slug);?>"><?php echo limit_words($row->tulisan_judul,3).'...';?></a></h5>
                        <p><?php echo limit_words($row->tulisan_isi,5).'...';?></p>
                    </div>
                    <hr>
                  <?php endforeach;?>
                </div>

            </div>
        </div>
    </div>
</section>

<!--============================= FOOTER =============================-->
<?php $this->load->view('depan/footer'); ?>