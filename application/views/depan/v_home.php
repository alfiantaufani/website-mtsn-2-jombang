<?php date_default_timezone_set('Asia/Jakarta'); $this->load->view('depan/header'); ?>

<section>
    <div class="slider_img layout_two">
        <div id="carousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <?php 
                    $cek = $this->db->query("SELECT COUNT(*) AS bnyk FROM tbl_slider")->result_array();
                    foreach ($cek as $d) {
                        $no = $d['bnyk'];
                        for ($no=0; $no < 3; $no++) { 
                ?>
                    <li data-target="#carousel" data-slide-to="<?=$no ?>" class="<?php if($no == 0){echo'active';}else{echo '';}?>"></li>';

                <?php   }
                    }
                ?>
                
            </ol>
            <div class="carousel-inner" role="listbox">
                <?php 
                    $no= 0;
                    $slider = $this->db->query("SELECT * FROM tbl_slider ORDER BY id ASC")->result_array();
                    foreach ($slider as $slide) :
                ?>
                <div class="carousel-item <?php if($no == 0){echo 'active';} ?>">
                    <img class="d-block" src="<?php echo base_url().'assets/images/slider/'.$slide['image'];?>" alt="First slide">
                    <div class="carousel-caption d-md-block">
                        <div class="slider_title">
                            <h1><?= $slide['caption'] ?></h1>
                            <h4><?= $slide['description'] ?></h4>
                            <div class="slider-btn">
                                <a href="<?= $slide['url'] ?>" class="btn btn-default">Selengkapnya</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php $no++; endforeach ?>
            </div>
            <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                <i class="icon-arrow-left fa-slider" aria-hidden="true"></i>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                <i class="icon-arrow-right fa-slider" aria-hidden="true"></i>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</section>
<!--//END HEADER -->
<!--============================= ABOUT =============================-->
<!-- <section class="clearfix about about-style2">
    <div class="container">
        <div class="row">
            <?php $query = $this->db->query("SELECT * FROM tbl_setting WHERE id=1")->result_array(); foreach ($query as $data) :?>
            <div class="col-md-9">
               <h2>Sambutan Kepala Sekolah</h2>
               <p><?= $data['text_home']; ?></p> 
            </div>
            <div class="col-md-3">
                <img src="<?php echo base_url().'theme/images/kepsek/'.$data['foto_kepsek'].''?>" class="img-fluid about-img" alt="foto Kepala Sekolah">
                <p class="text-center"><b><?= $data['nama_kepsek']; ?></b></p>
            </div>
            <?php endforeach ?>
        </div>
    </div>
</section> -->
<!--//END ABOUT -->
<!--============================= OUR COURSES =============================-->
<section class="our_courses">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Artikel Populer</h2>
            </div>
        </div>
        <div class="row">
          <?php foreach ($berita->result() as $row) :?>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <div class="courses_box mb-4">
                    <div class="course-img-wrap">
                        <img src="<?php echo base_url().'assets/images/'.$row->tulisan_gambar;?>" class="img-fluid" alt="courses-img">
                    </div>
                    <!-- // end .course-img-wrap -->
                    <a href="<?php echo site_url('artikel/'.$row->tulisan_slug);?>" class="course-box-content">
                        <h4 style="text-align:left;"><?php echo $row->tulisan_judul;?></h4>
                        <small><?php echo $row->tulisan_author;?> | <?php echo $row->tulisan_tanggal;?></small>
                    </a>
                </div>
            </div>
          <?php endforeach;?>
        </div> <br>
        <div class="row">
            <div class="col-md-12 text-center">
                <a href="<?php echo site_url('artikel');?>" class="btn btn-default btn-courses">Lihat Semua</a>
            </div>
        </div>
    </div>
</section>
<!--//END OUR COURSES -->
<!--============================= EVENTS =============================-->
<section class="event">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <h2>Pengumuman Terbaru</h2>
                <div class="event-img2">
                <?php foreach ($pengumuman->result() as $row) :?>
                <div class="row">
                    <div class="col-sm-3"> <img src="<?php echo base_url().'theme/images/announcement-icon.png'?>" class="img-fluid" alt="event-img"></div><!-- // end .col-sm-3 -->
                    <div class="col-sm-9"> <h3><a href="<?php echo site_url('pengumuman');?>"><?php echo limit_words($row->pengumuman_judul,6).'...';?></a></h3>
                      <span><?php echo $row->tanggal;?></span>
                      <p><?php echo limit_words($row->pengumuman_deskripsi,10).'...';?></p>

                    </div><!-- // end .col-sm-7 -->
                </div><!-- // end .row -->
                <?php endforeach;?>
                </div>
            </div>
            <div class="col-lg-6">
                <h2>Agenda Terbaru</h2>
                <div class="row">
                    <div class="col-md-12">
                      <?php foreach ($agenda->result() as $row):?>
                        <div class="event_date">
                            <div class="event-date-wrap">
                                <p><?php echo date("d", strtotime($row->agenda_tanggal));?></p>
                                <span><?php echo date("M. y", strtotime($row->agenda_tanggal));?></span>
                            </div>
                        </div>
                        <div class="date-description">
                            <h3><a href="<?php echo site_url('agenda');?>"><?php echo limit_words($row->agenda_nama,10).'...';?></a></h3>
                            <p><?php echo limit_words($row->agenda_deskripsi,10).'...';?></p>
                            <hr class="event_line">
                        </div>
                        <?php endforeach;?>

                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<!--//END EVENTS -->
<!--============================= DETAILED CHART =============================-->
<div class="detailed_chart">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-3 chart_bottom">
                <div class="chart-img">
                    <img src="<?php echo base_url().'theme/images/chart-icon_1.png'?>" class="img-fluid" alt="chart_icon">
                </div>
                <div class="chart-text">
                    <p><span class="counter"><?php echo $tot_guru;?></span> Guru
                    </p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 chart_bottom chart_top">
                <div class="chart-img">
                    <img src="<?php echo base_url().'theme/images/chart-icon_2.png'?>" class="img-fluid" alt="chart_icon">
                </div>
                <div class="chart-text">
                    <p><span class="counter"><?php echo $tot_siswa;?></span> Alumni
                    </p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 chart_top">
                <div class="chart-img">
                    <img src="<?php echo base_url().'theme/images/chart-icon_3.png'?>" class="img-fluid" alt="chart_icon">
                </div>
                <div class="chart-text">
                    <p><span class="counter"><?php echo $tot_files;?></span> Download
                    </p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="chart-img">
                    <img src="<?php echo base_url().'theme/images/chart-icon_4.png'?>" class="img-fluid" alt="chart_icon">
                </div>
                <div class="chart-text">
                    <p><span class="counter"><?php echo $tot_agenda;?></span> Agenda</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!--//END DETAILED CHART -->
<section class="our_courses">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Peta Lokasi</h2>
            </div>
        </div>
        <div class="col-md-12">
        <?php $query = $this->db->query("SELECT * FROM tbl_setting WHERE id=1")->result_array(); foreach ($query as $map) :?>
            <?= $map['maps']; ?>
        <?php endforeach;?>
        </div>
    </div>
</section>
<!--============================= FOOTER =============================-->
<?php $this->load->view('depan/footer'); ?>