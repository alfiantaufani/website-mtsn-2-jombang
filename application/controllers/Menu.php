<?php
class Menu extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('m_menus');
		$this->load->model('m_submenus');
	}

    public function index()
    {
        $data['menu'] = $this->db->get('tbl_menu')->result();
        $this->load->view('depan/header', $data);
    }
}