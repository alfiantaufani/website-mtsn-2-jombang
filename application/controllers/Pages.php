<?php
class Pages extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('m_pages');
		$this->load->model('m_pengunjung');
		$this->m_pengunjung->count_visitor();
	}
	function index(){
        $this->load->view('depan/v_home');
    }

    public function detail($slugs)
    {
        $slug   = htmlspecialchars($slugs,ENT_QUOTES);
		$query  = $this->db->query("SELECT * FROM tbl_page WHERE slug='$slug'");
		if($query->num_rows() > 0){
            $data       = $query->row_array();
			$id_halaman = $data['id'];
            
            $data_page['pages']     = $this->m_pages->get_pages($id_halaman);
            $data_page['title']     = $data['title'];
            $data_page['populer']   = $this->db->query("SELECT * FROM tbl_tulisan ORDER BY tulisan_views DESC LIMIT 5");
            $data_page['category']  = $this->db->get('tbl_kategori');
            $this->load->view('depan/pages/index', $data_page);
        }else{
            $this->load->view('errors/index');
        }
    }

    function kategori(){
        $kategori=str_replace("-"," ",$this->uri->segment(3));
        $query = $this->db->query("SELECT tbl_tulisan.*,DATE_FORMAT(tulisan_tanggal,'%d/%m/%Y') AS tanggal FROM tbl_tulisan WHERE tulisan_kategori_nama LIKE '%$kategori%' ORDER BY tulisan_views DESC LIMIT 5");
        if($query->num_rows() > 0){
            $x['data']=$query;
            $x['category']=$this->db->get('tbl_kategori');
             $x['populer']=$this->db->query("SELECT * FROM tbl_tulisan ORDER BY tulisan_views DESC LIMIT 5");
            $this->load->view('depan/pages/index',$x);
        }else{
            echo $this->session->set_flashdata('msg','<div class="alert alert-danger">Tidak Ada artikel untuk kategori <b>'.$kategori.'</b></div>');
            redirect('artikel');
        }
   }

   function search(){
       $keyword=str_replace("'", "", htmlspecialchars($this->input->get('keyword',TRUE),ENT_QUOTES));
       $query=$this->m_tulisan->cari_berita($keyword);
               if($query->num_rows() > 0){
                   $x['data']=$query;
                   $x['category']=$this->db->get('tbl_kategori');
                 $x['populer']=$this->db->query("SELECT * FROM tbl_tulisan ORDER BY tulisan_views DESC LIMIT 5");
         $this->load->view('depan/pages/index',$x);
             }else{
                echo $this->session->set_flashdata('msg','<div class="alert alert-danger">Tidak dapat menemukan artikel dengan kata kunci <b>'.$keyword.'</b></div>');
                redirect('artikel');
            }
   }
}