<?php
class Submenus extends CI_Controller{
    function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('administrator');
            redirect($url);
        };
		$this->load->model('m_submenus');
		$this->load->model('m_menus');
	}

	public function index(){
		$x['data'] = $this->m_submenus->get_all_submenus();
        $data['menus'] = $this->m_menus->get_all_menus();
		$x['title'] = "Sub Menu";
		$this->load->view('admin/submenus/index',$x);
	}

	public function create()
	{
		$x['title'] = "Tambah Sub Menu";
        $x['data'] = $this->m_menus->get_all_menus();
		$this->load->view('admin/submenus/create',$x);
	}

	public function store()
	{
		$nama_submenu = $this->input->post('nama_submenu');
		$cek = $this->db->query("SELECT * FROM tbl_submenu WHERE submenu_name='$nama_submenu' LIMIT 1");
		if($cek->num_rows() > 0){
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
				<b>Gagal!</b> Nama Sub Menu sudah ada <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
			');
			$referred_from = $this->session->userdata('referred_from'); 
			redirect('admin/content/submenus/create');
		}else{
			$data = [
				'submenu_name' => $nama_submenu,
				'submenu_link' => $this->input->post('link_submenu'),
				'menu_id' => $this->input->post('menu_id'),
			];
			$insert = $this->db->insert("tbl_submenu", $data);
			if($insert){
				echo $this->session->set_flashdata('msg','success');
				redirect('admin/content/submenus');
			}else{
				echo $this->session->set_flashdata('msg','error');
				redirect('admin/content/submenus');
			}
		}
	}

	public function edit($id)
	{
		$data['title'] = "Edit Sub Menu";
		$data['submenus'] = $this->m_submenus->get_submenus($id);
        $data['menus'] = $this->m_menus->get_all_menus();
		$this->load->view('admin/submenus/edit',$data);
	}

	public function update()
	{
		$id = $this->input->post('id');
		$data = [
            'submenu_name' => $this->input->post('nama_submenu'),
            'submenu_link' => $this->input->post('link_submenu'),
            'menu_id' => $this->input->post('menu_id'),
        ];
		$this->db->where('id', $id);
		$update = $this->db->update("tbl_submenu", $data);
		if($update){
			echo $this->session->set_flashdata('msg','success');
			redirect('admin/content/submenus');
		}else{
			echo $this->session->set_flashdata('msg','error');
			redirect('admin/content/submenus');
		}
	}

	public function delete()
	{
		$kode 	= strip_tags($this->input->post('kode'));
		$hsl	= $this->db->query("DELETE FROM tbl_submenu WHERE id='$kode'");
		if($hsl){
			echo $this->session->set_flashdata('msg','success-hapus');
			redirect('admin/content/submenus');
		}else{
			var_dump("Gagal hapus");
		}
	}
}