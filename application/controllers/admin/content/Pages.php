<?php
class Pages extends CI_Controller{
    function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('administrator');
            redirect($url);
        };
		$this->load->model('m_pages');
		$this->load->library('upload');
	}


	public function index(){
		$x['data']=$this->m_pages->get_all_pages();
		$x['title'] = "Halaman";
		$this->load->view('admin/pages/index',$x);
	}

	public function create()
	{
		$data['title'] = "Tambah Halaman";
		$this->load->view('admin/pages/create',$data);
	}

	public function store()
	{
		$judul		= strip_tags($this->input->post('xjudul'));
		$slug       = url_title($judul, '-', TRUE);

		$jdl = $this->input->post('xjudul');
		$cek_judul = $this->db->query("SELECT * FROM tbl_page WHERE title='$jdl' LIMIT 1");
		if($cek_judul->num_rows() > 0){
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
				<b>Gagal!</b> Judul halaman sudah ada <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
			');
			$referred_from = $this->session->userdata('referred_from'); 
			redirect('admin/content/pages/create');
		}else{
			$data = [
				'title' => $judul,
				'slug' 	=> $slug,
				'content' => $this->input->post('xisi'),
				'description' => $this->input->post('xdeskripsi'),
				'user_id' =>  $this->session->userdata('idadmin'),
			];
			$insert = $this->db->insert("tbl_page", $data);
			if($insert){
				echo $this->session->set_flashdata('msg','success');
				redirect('admin/content/pages');
			}else{
				echo $this->session->set_flashdata('msg','error');
				redirect('admin/content/pages');
			}
		}
		
	}

	public function edit($id_halaman)
	{
		$data['title'] = "Edit Halaman";
		$data['pages'] = $this->m_pages->get_pages($id_halaman);
		$this->load->view('admin/pages/edit',$data);
	}

	public function update()
	{
		$id_halaman = $this->input->post('id');
		$jdl = $this->input->post('xjudul');
		$cek_judul = $this->db->query("SELECT * FROM tbl_page WHERE title='$jdl' LIMIT 1");
		// if($cek_judul->num_rows() > 0){
		// 	$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
		// 		<b>Gagal!</b> Judul halaman sudah ada <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
		// 	');
		// 	$referred_from = $this->session->userdata('referred_from'); 
		// 	redirect('admin/content/pages/edit');
		// }else{
			$data = [
				'title' => $jdl,
				'content' => $this->input->post('xisi'),
				'description' => $this->input->post('xdeskripsi'),
			];
			$this->db->where('id', $id_halaman);
            $update = $this->db->update("tbl_page", $data);
			if($update){
				echo $this->session->set_flashdata('msg','success');
				redirect('admin/content/pages');
			}else{
				echo $this->session->set_flashdata('msg','error');
				redirect('admin/content/pages');
			}
		//}
	}

	public function delete(){
		$kode 	= strip_tags($this->input->post('kode'));
		$hsl	= $this->db->query("DELETE FROM tbl_page WHERE id='$kode'");
		if($hsl){
			echo $this->session->set_flashdata('msg','success-hapus');
			redirect('admin/content/pages');
		}else{
			var_dump("Gagal hapus");
		}
		
	}
}