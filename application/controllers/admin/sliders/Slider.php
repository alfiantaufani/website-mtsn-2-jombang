<?php
class Slider extends CI_Controller{
    function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('administrator');
            redirect($url);
        };
		$this->load->model('m_sliders');
		$this->load->library('upload');
	}

    public function index()
    {
        $x['data']=$this->m_sliders->get_all_sliders();
		$x['title'] = "Slider";
		$this->load->view('admin/sliders/index',$x);
    }

    public function store()
    {
		$judul = $this->input->post('judul');
		$cek = $this->db->query("SELECT * FROM tbl_slider WHERE caption='$judul' LIMIT 1");
		if($cek->num_rows() > 0){
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
				<b>Gagal!</b> Nama slider sudah ada <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
			');
			$referred_from = $this->session->userdata('referred_from'); 
			redirect('admin/sliders/slider');
		}else{
                $config['upload_path'] = './assets/images/slider'; //path folder
	            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
	            $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

	            $this->upload->initialize($config);
	            if(!empty($_FILES['filefoto']['name']))
	            {
	                if ($this->upload->do_upload('filefoto'))
	                {
                        $gbr = $this->upload->data();
                        //Compress Image
                        // $config['image_library']='gd2';
                        // $config['source_image']='./assets/images/slider/'.$gbr['file_name'];
                        // $config['create_thumb']= FALSE;
                        // $config['maintain_ratio']= FALSE;
                        // $config['quality']= '60%';
                        // $config['width']= 500;
                        // $config['height']= 400;
                        // $config['new_image']= './assets/images/slider/'.$gbr['file_name'];
                        // $this->load->library('image_lib', $config);
                        // $this->image_lib->resize();

                        $gambar = $gbr['file_name'];
                        $data = [
                            'image' => $gambar,
                            'caption' => $judul, 
                            'url' => $this->input->post('link'),
                            'description' => $this->input->post('deskripsi'),
                        ];
                        $insert = $this->db->insert("tbl_slider", $data);
                        if($insert){
                            echo $this->session->set_flashdata('msg','success');
                            redirect('admin/sliders/slider');
                        }else{
                            echo $this->session->set_flashdata('msg','error');
                            redirect('admin/sliders/slider');
                        }
                    }else{

                    }
                }else{
                    echo $this->session->set_flashdata('msg','error');
                    redirect('admin/sliders/slider');
                }
			
		}
    }

    public function delete()
    {
        $kode=$this->input->post('kode');
		$gambar=$this->input->post('gambar');
		$path='./assets/images/slider/'.$gambar;
		unlink($path);
		$hsl	= $this->db->query("DELETE FROM tbl_slider WHERE id='$kode'");
		if($hsl){
			echo $this->session->set_flashdata('msg','success-hapus');
			redirect('admin/sliders/slider');
		}else{
			echo $this->session->set_flashdata('msg','error');
            redirect('admin/sliders/slider');
		}
    }
}